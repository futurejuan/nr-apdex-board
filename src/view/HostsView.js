const Grid = require('./Grid');
const Row = require('./Row');

class HostsView {
    constructor(subtitle, rootElement, hostsList, maxTopApps) {
        this.maxTopApps = maxTopApps;
        this.rootElement = rootElement;
        this.title = "Apps by Host";
        this.subtitle = subtitle;
        this.checkboxText = "Show as list";
        this.tableType = Grid;
        this.hostsList = hostsList;
        this.root = document.getElementById("root");

        this.renderHeader = this.renderHeader.bind(this);
        this.updateView = this.updateView.bind(this);
        this.handleSwitchView = this.handleSwitchView.bind(this);
    }

    renderHeader() {
        const header = document.createElement('div');
        header.className += 'header';

        const titlesContainer = document.createElement('div');
        titlesContainer.className += 'titles-container';
        const title = document.createElement('h1');
        title.className = 'title';
        const titleText = document.createTextNode(this.title);
        title.appendChild(titleText);

        const subTitle = document.createElement('h2');
        subTitle.className = 'sub-title';
        const subtitleText = document.createTextNode(this.subtitle);
        subTitle.appendChild(subtitleText);

        titlesContainer.appendChild(title);
        titlesContainer.appendChild(subTitle);

        const switchViewContainer = document.createElement('div');
        switchViewContainer.className += 'switch-view';
        const checkbox = document.createElement('input');
        checkbox.setAttribute("type", "checkbox");

        checkbox.addEventListener( 'change', this.handleSwitchView);

        const switchText = document.createTextNode(this.checkboxText);

        switchViewContainer.appendChild(checkbox);
        switchViewContainer.appendChild(switchText);

        header.appendChild(titlesContainer);
        header.appendChild(switchViewContainer);
        
        this.root.appendChild(header);
    }

    handleSwitchView(e) {
        if(e.target.checked) {
            this.updateView(Row);
        } else {
            this.updateView(Grid);
        }
    }

    updateView(Table) {
        document.getElementById("hosts-body").outerHTML = "";

        if (Table) this.tableType = Table;
        
        this.renderBody();
    }

    renderBody() {
        const bodyRoot = document.createElement('div');
        bodyRoot.id = 'hosts-body';
        bodyRoot.className += 'body';
        this.root.appendChild(bodyRoot);

        const tables = Object.values(this.hostsList.hosts).map(host => new this.tableType(host, this.maxTopApps));
        tables.forEach(table => table.render(bodyRoot));
    }

    renderPageSkeleton() {
        const mainBody = document.createElement('div');
        mainBody.className += 'hosts-view';

        this.root.appendChild(mainBody);
    }

    render() {
        this.renderPageSkeleton();

        this.renderHeader();

        this.renderBody();
    }
}

module.exports = HostsView;