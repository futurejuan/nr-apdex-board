const Table = require('./Table');

class Grid extends Table {
    constructor(host, maxTopApps) {
        super(host, maxTopApps);
        this.host = host;
        this.maxTopApps = maxTopApps;
        this.className = 'grid-table-container';
    }
}

module.exports = Grid;