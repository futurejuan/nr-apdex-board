const Table = require('./Table');

class Row extends Table {
    constructor(host, maxTopApps) {
        super(host, maxTopApps);
        this.host = host;
        this.maxTopApps = maxTopApps;
        this.className = 'row-table-container';
    }
}

module.exports = Row;