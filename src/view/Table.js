class Table {
    constructor(host, maxTopApps) {
        this.host = host;
        this.maxTopApps = maxTopApps;
        this.className = 'table'
    }

    renderTableTitle(rootElement) {
        const tableTitleContainer = document.createElement('div');
        tableTitleContainer.className += 'host-title-container';

        const tableTitle = document.createElement('h3');
        const title = document.createTextNode(this.host.name);

        tableTitle.appendChild(title)
        tableTitleContainer.appendChild(tableTitle);
        rootElement.appendChild(tableTitleContainer)
    }

    renderTableList(rootElement) {
        const tableListContainer = document.createElement('div');
        tableListContainer.className += 'table-list-container';

        const table = document.createElement('ol');

        this.host.getTopApps(this.maxTopApps).forEach((app) => {
            const listElement = document.createElement('li');
            const apdexElement = document.createElement('p');
            apdexElement.className += 'apdex';
            apdexElement.appendChild(document.createTextNode(app.apdex))
            const appNameElement = document.createElement('p');
            appNameElement.appendChild(document.createTextNode(app.name));
            listElement.addEventListener('click', () => {
                const appInfoText = `Apdex: ${app.apdex}\n\nApp: ${app.name}\n\nVersion: ${app.version}`;
                window.alert(appInfoText);
            })

            listElement.appendChild(apdexElement);
            listElement.appendChild(appNameElement);
            table.appendChild(listElement);
        });

        tableListContainer.appendChild(table);
        rootElement.appendChild(tableListContainer);
    }
    
    render(rootElement) {
        const tableSkeleton = document.createElement('div');
        tableSkeleton.className += this.className;
        const tableBlody = document.createElement('div');
        tableBlody.className += 'table-body';

        tableSkeleton.appendChild(tableBlody);
        rootElement.append(tableSkeleton);

        this.renderTableTitle(tableBlody);
        this.renderTableList(tableBlody);
    }
}

module.exports = Table;