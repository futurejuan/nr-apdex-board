const AppsList = require('../appsList/appsList');

class Host {
    constructor(hostName) {
        if (!hostName) throw new Error('Host constructor called without hostName argument');

        this.apps = new AppsList();
        this.name = hostName;
    }

    add(app) {
        if (!app) throw new Error('add method called without appData argument');

        this.apps.set(app);
        return this;
    }

    remove(appData) {
        if (!appData) throw new Error('add method called without appData argument');

        this.apps.delete(appData);
        
        return this;
    }

    isEmpty() {
        return this.apps.isEmpty();
    }

    getTopApps(maxAmmount) {
        return this.apps.getN(maxAmmount);
    }
}

module.exports = Host;