const { expect } = require('chai');
const { describe, it } = require('mocha');
const sinon = require('sinon');
const proxyquire = require('proxyquire');

const sandbox = sinon.createSandbox();
const AppsList = require('../appsList/appsList');
const Host = proxyquire('./host', { '../appsList/appsList': 
    sandbox.spy(AppsList.prototype, 'constructor')
});

const mockedHostName = 'a hostname';
const mockedApdex = 68;
const mockedContributors = ["Edwin Reinger", "Ofelia Dickens", "Hilbert Cole", "Helen Kuphal", "Maurine McDermott Sr."];
const mockedHost = ["7e6272f7-098e.dakota.biz", "9a450527-cdd9.kareem.info", "e7bf58af-f0be.dallas.biz"];
const mockedName = "Small Fresh Pants - Kautzer - Boyer, and Sons";
const mockedVersion = 7;

const mockedApp = {
    apdex: mockedApdex,
    contributors: mockedContributors,
    host: mockedHost,
    name: mockedName,
    version: mockedVersion
};

describe('Host', () => {
    beforeEach(() => sandbox.reset());

    describe('constructor()', () => {

        beforeEach(() => sandbox.reset());

        it('Should create a Host instance', () => {
            const host = new Host(mockedHostName);

            expect(host).to.be.instanceOf(Host);
        });

        it('should create a Host with given hostName', () => {
            const host = new Host(mockedHostName)
    
            expect(host.name).to.equal(mockedHostName);
        });

        it('Should throw error when no hostName is given', () => {
            const unexpectedError = 'A Host was created without a hostName';

            try {
                const host = new Host();
                throw new Error(unexpectedError)
            } catch (e) {
                expect(e.message).to.not.equal(unexpectedError);
            }
        });
    })


    describe('add()', () => {
        beforeEach(() => sandbox.reset());

        it('Should throw error when no appData is given', () => {
            const unexpectedError = 'add didnt fail when called without an app';
            const host = new Host(mockedHostName);

            try {
                host.add();
                throw new Error(unexpectedError)
            } catch (e) {
                expect(e.message).to.equal('add method called without appData argument');
            }
        })

        it('should call `set` on its appsList when given an app', () => {
            const host = new Host(mockedHostName);
            sandbox.spy(host.apps, 'set');

            host.add(mockedApp);

            expect(host.apps.set.calledWith(mockedApp)).to.be.true;
        })
    })

    describe('remove()', () => {
        it('Should throw error when no appData is given', () => {
            const unexpectedError = 'remove didnt fail when called without an app';
            const host = new Host(mockedHostName);

            host.add(mockedApp);
            
            try {
                host.remove();
                throw new Error(unexpectedError)
            } catch (e) {
                expect(e.message).to.equal('add method called without appData argument');
            }
        })

        it('should remove the given app from the Host', () => {
            const mockedApp2 = { ...mockedApp, name: 'mocked-app-2'};
            const mockedApp3 = { ...mockedApp, name: 'mocked-app-3'};

            const host = new Host(mockedHostName);

            host.add(mockedApp3);
            host.add(mockedApp2);
            host.add(mockedApp);
            
            host.remove(mockedApp2);
            expect(host.apps.getApp(mockedApp2)).to.be.undefined;
        })
    })

    describe('getTopApps()', () => {
        it('should return an empty array when host has no apps', () => {
            const host = new Host(mockedHostName);

            expect(host.getTopApps()).to.deep.equal([]);
        })
        
        it('should return an array sortered by apdex', () => {
            const topApdexApp = { ...mockedApp, version: 1, apdex: 99 };
            const secondApdexApp = { ...mockedApp, version: 2, apdex: 80};
            const thirdApdexApp = { ...mockedApp, version: 3, apdex: 70 };

            const expectedResult = [topApdexApp, secondApdexApp, thirdApdexApp];

            const host = new Host(mockedHostName);

            host.add(secondApdexApp);
            host.add(topApdexApp);
            host.add(thirdApdexApp);

            expect(host.getTopApps(15)).to.deep.equal(expectedResult);
        })
    })
})