class App {
    constructor({ apdex, contributors, host, name, version} = {}) {
        if (!name) throw new TypeError(`No name provided for app`);
        if (!apdex) throw new TypeError(`No apdex provided for app ${name}`);
        if (!(host && host.length)) throw new TypeError(`No host provided for app ${name}`);
        if (!version) throw new TypeError(`No version provided for app ${name}`);

        this.apdex = apdex;
        this.contributors = contributors;
        this.host = Array.isArray(host) ? host : [host];;
        this.name = name;
        this.version = version;
    }

    toJson() {
        return {
            apdex: this.apdex,
            contributors: this.contributors,
            host: this.host,
            name: this.name,
            version: this.version,
        }
    }
}

module.exports = App;