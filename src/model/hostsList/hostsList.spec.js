const { expect } = require('chai');
const { describe, it } = require('mocha');
const sinon = require('sinon');
const proxyquire = require('proxyquire');

const sandbox = sinon.createSandbox();
const Host = require('../host/host');
const HostsList = proxyquire('./hostsList', { '../host/host': sandbox.spy(Host.prototype, 'constructor') });

const mockedHostName = 'a hostname';
const mockedApdex = 68;
const mockedContributors = ["Edwin Reinger", "Ofelia Dickens", "Hilbert Cole", "Helen Kuphal", "Maurine McDermott Sr."];
const mockedHost = ["7e6272f7-098e.dakota.biz", "9a450527-cdd9.kareem.info", "e7bf58af-f0be.dallas.biz"];
const mockedName = "Small Fresh Pants - Kautzer - Boyer, and Sons";
const mockedVersion = 7;

const mockedApp = {
    apdex: mockedApdex,
    contributors: mockedContributors,
    host: mockedHost,
    name: mockedName,
    version: mockedVersion
};

describe('HostsList', () => {
    describe('constructor()', () => {
        it('Should create a HostsList instance', () => {
            const hostsList = new HostsList([mockedApp]);

            expect(hostsList).to.be.instanceOf(HostsList);
        });

        it('should create a HostsList instance and store each given app host', () => {
            const hostsList = new HostsList([{ ...mockedApp, host: ['one', 'two'] }]);

            expect(Object.keys(hostsList.hosts)).to.have.length(2);
        });
    })

    describe('add()', () => {
        it('Should throw error when no appData is given', () => {
            const unexpectedError = 'add didnt fail when called without an app';
            const hostsList = new HostsList([mockedApp]);

            try {
                hostsList.add();
                throw new Error(unexpectedError)
            } catch (e) {
                expect(e.message).to.not.equal(unexpectedError);
            }
        })
        
        it('should create a host if no previous one matches for the given app.host', () => {
            const hostsList = new HostsList([mockedApp]);

            hostsList.add({ ...mockedApp, host: [mockedHostName] });
            expect(Host.prototype.constructor.calledWith(mockedHostName)).to.be.true;
        })

        it('should not create a host if a previous one exists for the given app.host', () => {
            const hostsList = new HostsList([mockedApp]);
            const mockedAppName = 'mocked app name';
            hostsList.hosts = [new Host('mockedHostName')];


            hostsList.add({ ...mockedApp, name: mockedAppName, host: [mockedHostName] });

            expect(Host.prototype.constructor.calledWith(mockedAppName)).to.be.false;
        })

        it('should not fail if given app.host is a string instead of array', () => {
            const hostsList = new HostsList([mockedApp]);

            hostsList.add({ ...mockedApp, host: mockedHostName });
            expect(Host.prototype.constructor.calledWith(mockedHostName)).to.be.true;
        })

        it('should fail if given app has no hosts', () => {
            const unexpectedError = 'add didnt fail when given app had no hosts';
            const hostsList = new HostsList([mockedApp]);

            try {
                hostsList.add({ ...mockedApp, host: null });
                throw new Error(unexpectedError)
            } catch (e) {
                expect(e.message).to.not.contain(unexpectedError);
            }
        })
    })

    describe('remove()', () => {
        it('Should throw error when no appData is given', () => {
            const unexpectedError = 'getHost didnt fail when called without an app';
            const hostsList = new HostsList([mockedApp]);

            try {
                hostsList.remove();
                throw new Error(unexpectedError)
            } catch (e) {
                expect(e.message).to.equal('remove method called without appData argument');
            }
        })

        it('should succesfully remove an app from all its hosts', () => {
            const hostsList = new HostsList([{ name: 'someUnusedName', host: ['mockedHost1', 'mockedHost2'], apdex: 2, version: 3 }]);

            let appToRemove = { ...mockedApp, host: ['mockedHost1', 'mockedHost2']};
            hostsList.add({ ...mockedApp, name: 'someUnusedName', host: ['mockedHost1', 'mockedHost2']});
            hostsList.add(appToRemove);
            

            hostsList.remove(appToRemove);
            
            expect(Object.keys(hostsList.hosts).length).to.equal(2);
            Object.values(hostsList.hosts).forEach((host) => {
                expect(host.apps.getApp(appToRemove)).to.be.undefined;
            })
        })

        it('should remove hosts when they have no apps', () => {
            const appsToRemove = [
                { name: 'an app', host: ['host'], version: 3, apdex: 3 },
                { ...mockedApp, host: ['mockedHost1', 'mockedHost2']}
            ];

            const hostsList = new HostsList(appsToRemove);
            
            appsToRemove.forEach(app => hostsList.remove(app))
            
            expect(Object.keys(hostsList.hosts).length).to.equal(0);
        })

        it('should return null', () => {
            const hostsList = new HostsList([mockedApp]);
            const mockedHost = (new Host('mockedHostName')).add(mockedApp);

            hostsList.hosts = [mockedHost];

            expect(hostsList.remove(mockedApp)).to.be.null;
        })

        it('should return null when given app wasnt in the host', () => {
            const hostsList = new HostsList([mockedApp]);

            expect(hostsList.remove(mockedApp)).to.be.null;
        })
    })
})