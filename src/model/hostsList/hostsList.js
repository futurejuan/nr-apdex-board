const Host = require('../host/host');
const App = require('../app/app');

class HostsList {
    constructor(appsData) {
        this.hosts = {};
        appsData.forEach(appData => this.add(appData));
    }

    add(appData) {
        const app = new App(appData);

        app.host.forEach((hostName) => {
            let host = this.getHost(hostName)

            if (!host) {
                host = new Host(hostName);
                this.hosts[hostName] = host;
            } 

            host.add(app);
        });
        
        return this;
    }

    remove(appData) {
        if (!appData) throw new Error('remove method called without appData argument');
        if (!appData.host || !appData.host.length) throw new Error('Given app has no hosts');

        const appHosts = Array.isArray(appData.host) ? appData.host : [appData.host];


        appHosts.forEach((hostName) => {
            let storedHost = this.getHost(hostName)

            if (!storedHost) {
                return;
            }

            storedHost.remove(appData);

            if (storedHost.isEmpty()) {
                this.removeHost(hostName)
            }
        })

        return null;
    }

    removeHost(hostName) {
        delete this.hosts[hostName];
    }

    getHost(hostName) {
        if (!hostName) throw new Error('getHost method called without hostName argument');

        return this.hosts[hostName];
    }
}

module.exports = HostsList;