const App = require('../app/app');

class AppsList {
    constructor(apps) {
        this.apps = {};
    }

    set(app) {
        if (this.apps[app.apdex]) {
            this.apps[app.apdex].push(app)
        } else {
            this.apps[app.apdex] = [app]
        }
    }

    delete(appData) {

        this.apps[appData.apdex] = this.apps[appData.apdex].filter(storedApp => 
            !(storedApp.name === appData.name && storedApp.version === appData.version) 
        );
    }

    isEmpty() {
        return this.getN(1).length === 0;
    }

    getApp(appToRetrieve) {
        return this.apps[appToRetrieve.apdex].find(app => app.name === appToRetrieve.name);
    }

    getN(ammount) {
        const topApdex = Object.keys(this.apps).sort((a,b) => b-a);
        let topApps = [];

        topApdex.forEach((topApd) => {
            if (topApps.length < ammount) {
                topApps = topApps.concat(this.apps[topApd].slice(0, ammount - topApps.length))
            }
        });

        return topApps;
    }
}

module.exports = AppsList;