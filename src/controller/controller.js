const HostsList = require('../model/hostsList/hostsList');
const HostsView = require('../view/HostsView');
const Grid = require('../view/Grid');
const Row = require('../view/Row');

class Controller {
    constructor(appsData) {
        this.maxTopApps = 25;
        this.hostsList = new HostsList(appsData);

        this.view = new HostsView(
            "for user averylongemailadress@companyname.com", 
            "root", 
            this.hostsList,
            this.maxTopApps,
            Grid
        );

        this.view.render();
    }

    getTopAppsByHost(hostName) {
        const host = this.hostsList.getHost(hostName);
        return host.getTopApps(this.maxTopApps);
    }

    addAppToHosts(app) {
        try {
            this.hostsList.add(app);
            this.view.updateView.call(this.view);
        } catch (e) {
            window.alert(e);
        }
    }

    removeAppFromHosts(app) {
        try {
            this.hostsList.remove(app);
            this.view.updateView.call(this.view);
        } catch (e) {
            window.alert(e);
        }
    }

    addApp(app) {
        this.apps.push(app);
    }
}

module.exports = Controller;