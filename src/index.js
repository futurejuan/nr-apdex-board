const hostAppData = require('./host-app-data.json');
const Controller = require('./controller/controller');

var start = new Date()

const controller = new Controller(hostAppData);
window.getTopAppsByHost = controller.getTopAppsByHost.bind(controller);
window.addAppToHosts = controller.addAppToHosts.bind(controller);
window.removeAppFromHosts = controller.removeAppFromHosts.bind(controller);

var end = new Date() - start;

console.info('Execution time: %dms', end)
