const assert = require('assert');
const actions = require('./actions');
const browserUtils = require('./utils/browser');

const hosts = ['7e6272f7-098e.dakota.biz', '9a450527-cdd9.kareem.info'];
const appToAdd = {name: "Woaaaa", host: hosts, version: 4, apdex: 100};

describe('AddAppFlow', () => {
    before(() => {
        browser.url('http://localhost:8080/');
    });

    describe('When the user tries to add an app with top apdex', () => {
        actions.addApp('Then the app is added', appToAdd);

        describe('Then the app', () => {
            it('should be rendered to its hosts', () => {
                const hostsWhereAppWasListed = browser.execute(browserUtils.getHostsWhereAppIsListed, appToAdd)

                assert.deepEqual(appToAdd.host, hostsWhereAppWasListed);
            })
        })
    });

    describe('When the user tries to add an app with low apdex', () => {
        const mockedApp = { name: 'test', host: hosts, apdex: 3 };

        actions.addApp('Then the app is added', mockedApp);

        describe('Then the app isnt in the top list', () => {
            it('should not render to its hosts', () => {
                const hostsWhereAppWasListed = browser.execute(browserUtils.getHostsWhereAppIsListed, mockedApp)
                
                assert.strictEqual(hostsWhereAppWasListed.length, 0);
            })
        })
    })

    describe('When the user tries to add an app with no apdex', () => {
        const mockedApp = { name: 'test', host: hosts };

        actions.addApp('Then the app is added', mockedApp);

        describe('Then the app throws an error', () => {
            it('should open an alert', () => {
                assert.strictEqual(browser.isAlertOpen(), true);
            })
        })
    })
});