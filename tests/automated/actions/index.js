const assert = require('assert');

const toggleView = (testTitle) => describe(testTitle || 'ToggleView', () => {
    it('it should have a checkbox to switch view', () => {
        const checkbox = browser.$('.switch-view').$('input[type="checkbox"]');

        assert.strictEqual(checkbox.isExisting(), true);
    });

    it('it should switch ', () => {
        const checkbox = browser.$('.switch-view').$('input[type="checkbox"]');
        let initialCheckedState = checkbox.getProperty('checked');

        checkbox.click();
        assert.strictEqual(checkbox.getProperty('checked'), !initialCheckedState);
    });
});

const addApp = (testTitle, appToAdd) => describe(testTitle || 'AddApp', () => {
    it('should have a way to add an app', () => {
        const typeofAddAppToHosts = browser.execute(() => {
            return typeof window.addAppToHosts;
        })
        assert.strictEqual(typeofAddAppToHosts, "function");
    })
    it('should add an app', () => {
        browser.execute((appToAdd) => {
            return window.addAppToHosts(appToAdd);
        }, appToAdd);
    })
});

const removeApp = (testTitle, appToRemove) => describe(testTitle || 'RemoveApp', () => {
    it('should have a way to remove an app', () => {
        const typeofRemoveAppToHosts = browser.execute(() => {
            return typeof window.removeAppFromHosts;
        })
        assert.strictEqual(typeofRemoveAppToHosts, "function");
    })
    it('should remove an app', () => {
        browser.execute((appToRemove) => {
            return window.removeAppFromHosts(appToRemove);
        }, appToRemove);
    })
});

module.exports = {
    toggleView,
    addApp,
    removeApp,
}