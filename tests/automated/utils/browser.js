const getHostsWhereAppIsListed = (appToAdd) => {
    
    let matchingHosts = [];

    document.querySelectorAll('.table-list-container > ol > li').forEach(el => {
        const htmlAppText = el.childNodes[1].outerText;
        if (htmlAppText === appToAdd.name) {
            const titleContainer = el.parentNode.parentNode.parentNode;

            matchingHosts.push(titleContainer.childNodes[0].outerText)
        }
        
    });

    return matchingHosts;
};


module.exports = {
    getHostsWhereAppIsListed,
}