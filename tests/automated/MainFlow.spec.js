const assert = require('assert');
const GridRenderTest = require('./views/GridRender');
const ListRenderTest = require('./views/ListRender');
const actions = require('./actions');

describe('MainFlow', () => {
    const pageTitle = 'Apps by Host';

    before(() => {
        browser.url('http://localhost:8080/');
    });

    describe('When the page opens', () => {
        it('should have the right page title', () => {
            const title = browser.getTitle();
            assert.strictEqual(title, pageTitle);
        });
    
        it('should display the right title in the view', () => {
            const renderedTitle = browser.$('.title').getText();
    
            assert.strictEqual(renderedTitle, pageTitle);
        });
    })

    GridRenderTest('Then it renders a grid');

    actions.toggleView('Then the user tries to switch view');

    ListRenderTest('Then it renders a list');

    actions.toggleView('Then the user tries to switch view back to grid');

    GridRenderTest('Then it renders a grid again');
});