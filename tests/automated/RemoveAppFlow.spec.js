const assert = require('assert');
const actions = require('./actions');
const browserUtils = require('./utils/browser');

const hosts = [
    "95b346a0-17f4.abbigail.name",
    "128406fc-0d3f.tiana.biz",
    "92116865-5462.conor.com",
];

const appToRemove = {
    host: hosts,
    name: "Unbranded Steel Towels - Abbott LLC, Inc",
    version: 1,
    apdex: 100
}

describe('RemoveAppFlow', () => {
    before(() => {
        browser.url('http://localhost:8080/');
    });

    describe('When the user tries to remove an app with hosts', () => {
        actions.removeApp('Then the app gets removed', appToRemove);

        describe('Then the view reflects the deletion', () => {
            it('should not be rendered to its hosts', () => {
                const hostsWhereAppWasListed = browser.execute(browserUtils.getHostsWhereAppIsListed, appToRemove)

                assert.deepEqual(hostsWhereAppWasListed.length, 0);
            })
        })
    });

    describe('When the user tries to remove an app with no matching version in hosts', () => {
        actions.removeApp('Then the app gets removed', { appToRemove, version: '9000' });

        describe('Then an error happens', () => {
            it('should throw an alert', () => {
                assert.strictEqual(browser.isAlertOpen(), true);
            })
        });
    })

    describe('When the user tries to remove an app with no hosts', () => {
        actions.removeApp('Then the app gets removed', { appToRemove, host: [] });

        describe('Then an error happens', () => {
            it('should throw an alert', () => {
                assert.strictEqual(browser.isAlertOpen(), true);
            })
        });
    })
});