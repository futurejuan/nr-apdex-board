const assert = require('assert');

const GridRenderTest = (testTitle) => describe(testTitle || 'GridRender', () => {

    it('should use a grid view', () => {
        const defaultTableType = browser.$('.grid-table-container');

        assert.strictEqual(defaultTableType.isExisting(), true);
    });

    it('should not be rendering any row views', () => {
        const rowViewElement = browser.$('.row-table-container');

        assert.strictEqual(rowViewElement.isExisting(), false);
    });

    it('should be rendering all 10 hosts', () => {
        const hostTitles = browser.$$('.host-title-container');

        assert.strictEqual(hostTitles.length, 10);
    });

    it('should be rendering not more than 25 apps per host', () => {
        const hostTitles = browser.$$('ol').forEach(list => {
            const totalApps = list.$$('li').length;

            assert.ok(totalApps <= 25);
        });
    });
});

module.exports = GridRenderTest;