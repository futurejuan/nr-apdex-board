const express = require('express');
const app = express();
const fs = require('fs');
const path = require('path');

let html; 

const port = process.env.npm_config_port || 8080;

app.get('/', (req, res, next) => {
    res.sendFile(path.resolve(__dirname, './dist/index.html'));
});

app.use(express.static('./dist'))


app.listen(port, () => { console.log(`Listening on port ${port}`) })

module.exports = app;