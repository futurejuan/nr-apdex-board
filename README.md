# 🚀 Welcome to the code challenge!

## Running the app:

1- Install all dependencies:
```
npm install 
```

2- Build all assets:
```
npm run build
```

3- Start the server:
```
npm run start
```

## Running tests:

### Unit tests:
```
npm run test:unit
```

### Automated tests:
```
npm run test:e2e
```

### Running all together
> Not recommended since it might leave an open port when exiting
```
npm run test
```

## Big-O

### window.getTopAppsByHost:
`O(1)` -> we're ordering an array of apdex's (maximum of 100 elements) and then just reading the first 25 elements. At most we'll iterate 125 times (100 for ordering apdexes and 25 for accessing the first 25 elements). Complexity won't grow upon ammount of apps.
This is the most used function since it will be called on each page updates, weather from adding or removing apps.

### window.addAppToHosts:
`O(N)` -> being N the ammount of hosts where the app is to be added. Knowing the domain this shouldn't grow much since apps usually aren't added to many hosts, as so this algorithm is **O(1)** since its complexity is independant from ammount of apps or hosts already stored.

### window.removeAppFromHosts:
`O(N)` -> we'll be iterating over the apps of the same index of the given app. We could have used a hashmap to directly remove the desired element (delete apps[apdex][appName]), but then we would have had to penalize the most used fn of getTopAppsByHost,



